/**************************************************************************
 *   grep.c - by Shaun Fernandez,111608058 shaunfernandez98@gmail.com    *
 *                                                                        *
 *   Copyright (C) 2017  Shaun Benjamin Fernandez              		  *
 *                                                                        *
 *   GREP is free software: you can redistribute it and/or modify         *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   GREP is distributed in the hope that it will be useful,              *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <dirent.h>
#include <ctype.h>

//Colors
#define RED   "\x1B[31;1m"
#define GREEN   "\x1b[32m"
#define GRAY   "\x1B[37;3m"
#define RESET "\x1B[0m"

//////OPTIONS///////
 	int grep_f;
	int grep_v;
	int grep_H;
	int grep_h;
	int grep_r;
	int grep_i;
	int grep_q;
	int grep_w;
	int grep_c;
	int grep_m, m_number;
	int grep_b;
//////OPTIONS///////




//Function space
void grep(char *pattern, char *filename);
void grepv(char *pattern, char *filename);
void printgrep(int type, char *filename, int byteoffset)
{
	if(type==0)
	{
		printf(GRAY "%s:", filename);
		printf(RESET "");
	}
	else if(type==1)
	{
		printf("grep.c:%d:", byteoffset);
	}
}
int checkbinary(char *filename)
{
	FILE *fp;
	int c;

	fp = fopen(filename, "r");
	while((c = fgetc(fp)) != EOF) 
	{
	        if (c >= 0x00 && c<=0x7F) 
	        {
	        }
	        else
	        {
	        	fclose(fp);
	        	printf(GRAY "%s:",filename);
	        	printf(RESET" File is Binary\n");
	            return 1;		//Binary
	        }
	 }

	fclose(fp);
	return 0;				//text file
}

void grep(char *pattern, char *filename)
{
	if(checkbinary(filename)==1)
	{
		return ;
	}
	FILE* file = fopen(filename, "r");
	if(file == NULL)
	{
		return ;
	}
	char line[1024];
	if(grep_H == 0 && grep_h == 0)
	{
		grep_h=1;
	}

	if(m_number==0 && grep_m==1)
	{
		printf("grep: Invalid max count\n");
		exit(0);
	}
	if(filename[0]=='.' && filename[1]=='/')
		filename+=2;
	int i, pos;
	char *lineptr,*ptr;
	int byteposition=0;
	if((grep_w==0 && grep_c==0 && grep_m==0))
	{
		while(fgets(line, sizeof(line), file))
		{
			if(grep_i==0 && strstr(line, pattern)==NULL)
			{
				if(grep_b==1)
					byteposition=ftell(file);
				continue;
			}
			else if(grep_i==1 && strcasestr(line, pattern)==NULL)
			{
				if(grep_b==1)
					byteposition=ftell(file);
				continue;
			}
			lineptr = line;
			if(grep_i==1)
				ptr=strcasestr(lineptr, pattern);
			else
				ptr=strstr(lineptr, pattern);
			if(grep_H==1)
				printgrep(0, filename, 0);
			if(grep_b==1)
			{
				printf(GREEN "%d:",byteposition);
				printf(RESET "");
			}
			while(ptr!=NULL)
			{
				pos = ptr - lineptr;
				for(i=0;i<pos;i++)
				{
					putchar(lineptr[i]);
				}

				for(i=0;i<strlen(pattern);i++)
				{
					printf(RED "%c", ptr[i]);
					printf(RESET "");
				}

				lineptr= lineptr + strlen(pattern) + pos;
				if(grep_i==1)
					ptr=strcasestr(lineptr, pattern);
				else
					ptr=strstr(lineptr, pattern);
			}
			printf("%s", lineptr);
			if(grep_b==1)
				byteposition=ftell(file);
		}

	}
	int matchcount=0;
	if(grep_w==0 && (grep_c==1 || grep_m==1))
	{
		while(fgets(line, sizeof(line), file))
		{
			if(grep_i==0 && strstr(line, pattern)==NULL)
			{
				if(grep_b==1)
					byteposition=ftell(file);
				continue;
			}
			else if(grep_i==1 && strcasestr(line, pattern)==NULL)
			{
				if(grep_b==1)
					byteposition=ftell(file);
				continue;
			}
			lineptr = line;
			if(grep_i==1)
				ptr=strcasestr(lineptr, pattern);
			else
				ptr=strstr(lineptr, pattern);
			matchcount++;
			if(grep_H==1 && grep_c==0)
				printgrep(0, filename, 0);
			if(grep_b==1 && grep_c==0)
			{
				printf(GREEN "%d:",byteposition);
				printf(RESET "");
			}
			while(ptr!=NULL)
			{
				pos = ptr - lineptr;
				if(grep_c==0)
				{
					for(i=0;i<pos;i++)
					{
						putchar(lineptr[i]);
					}
					for(i=0;i<strlen(pattern);i++)
					{
						printf(RED "%c", ptr[i]);
						printf(RESET "");
					}
				}
				lineptr= lineptr + strlen(pattern)+pos;
				if(grep_i==1)
					ptr=strcasestr(lineptr, pattern);
				else
					ptr=strstr(lineptr, pattern);
			}
			if(grep_c==0)
				printf("%s", lineptr);
			if(grep_m==1 && m_number==matchcount)
				break;
			if(grep_b==1)
				byteposition=ftell(file);
		}
		if(grep_H==1 && grep_c==1)
			printgrep(0, filename, 0);
		if(grep_c==1)
			printf("%d\n", matchcount);
	}
}



int atleastonewordi(char *line, char *pattern)
{
	char *ptr;
	int i , pos;
	char *lineptr=line;
	int lengthpattern=0;
	for(i=0;pattern[i]!='\0';i++)		//Getting length of pattern
	{
		lengthpattern++;
	}
	ptr=strcasestr(lineptr, pattern);
	while(ptr!=NULL)					//Used to find atleast one WORD not string within a sentence
	{
		pos = ptr - lineptr;
		if(ptr==lineptr)
		{
			if(isalnum(ptr[lengthpattern])==0 && ptr[lengthpattern]!='_')			//Words are defined as having no character or number before or after them
			{
				return 1;
			}
		}
		else if(ptr!=NULL)
		{
			if(isalnum(ptr[-1])==0 && ptr[-1]!='_' && isalnum(ptr[lengthpattern])==0 && ptr[lengthpattern]!='_')
			{
				return 1;
			}
		}
		lineptr = lineptr + pos + strlen(pattern);
		ptr=strcasestr(lineptr, pattern);
	}
	return 0;								//Meaning no word has been found
}
int atleastoneword(char *line, char *pattern)			//case sensitive
{
	char *ptr;
	int i , pos;
	char *lineptr=line;
	int lengthpattern=0;
	for(i=0;pattern[i]!='\0';i++)		//Getting length of pattern
	{
		lengthpattern++;
	}
	ptr=strstr(lineptr, pattern);
	while(ptr!=NULL)					//Used to find atleast one WORD not string within a sentence
	{
		pos = ptr - lineptr;
		if(ptr==lineptr)
		{
			if(isalnum(ptr[lengthpattern])==0 && ptr[lengthpattern]!='_')			//Words are defined as having no character or number before or after them
			{
				return 1;
			}
		}
		else if(ptr!=NULL)
		{
			if(isalnum(ptr[-1])==0 && ptr[-1]!='_' && isalnum(ptr[lengthpattern])==0 && ptr[lengthpattern]!='_')
			{
				return 1;
			}
		}
		lineptr = lineptr + pos + strlen(pattern);
		ptr=strstr(lineptr, pattern);
	}
	return 0;								//Meaning no word has been found
}
void grepw(char *pattern, char *filename)
{
	if(checkbinary(filename)==1)
	{
		return ;
	}
	FILE* file = fopen(filename, "r");
	if(file == NULL)
	{
		return ;
	}
	char line[1024];
	if(filename[0]=='.' && filename[1]=='/')
		filename+=2;
	if(grep_H == 0 && grep_h == 0)
	{
		grep_h=1;
	}
	if(m_number==0 && grep_m==1)
	{
		printf("grep: Invalid max count\n");
		exit(0);
	}
	int i, pos;
	char *lineptr,*ptr;
	int flag;
	int byteposition=0;


	// no c and m
	if((grep_w==1 && grep_c==0 && grep_m==0))
	{
		while(fgets(line, sizeof(line), file))
		{
			if(grep_i==0 && strstr(line, pattern)==NULL)
			{
				if(grep_b==1)
					byteposition=ftell(file);
				continue;
			}
			else if(grep_i==1 && strcasestr(line, pattern)==NULL)
			{
				if(grep_b==1)
					byteposition=ftell(file);
				continue;
			}
			lineptr = line;

			if(grep_i==0 && atleastoneword(line, pattern)==0)
				continue;
			else if(grep_i==1 && atleastonewordi(line, pattern)==0)
				continue;
			if(grep_i==1)
				ptr=strcasestr(lineptr, pattern);
			else
				ptr=strstr(lineptr, pattern);
			if(grep_H==1)
				printgrep(0, filename, 0);
			if(grep_b==1)
			{
				printf(GREEN "%d:",byteposition);
				printf(RESET "");
			}
			if(ptr==line)
				flag=10;
			while(ptr!=NULL)
			{
				pos = ptr - lineptr;
				if((flag==10 && isalnum(ptr[strlen(pattern)])==0 && ptr[strlen(pattern)]!='_') || (isalnum(ptr[-1])==0 && isalnum(ptr[strlen(pattern)])==0 && ptr[-1]!='_' && ptr[strlen(pattern)]!='_')) 
				{
					for(i=0;i<pos;i++)
					{
						putchar(lineptr[i]);
					}

					for(i=0;i<strlen(pattern);i++)
					{
						printf(RED "%c", ptr[i]);
						printf(RESET "");
					}
				}
				else
				{
					for(i=0;i<pos;i++)
					{
						putchar(lineptr[i]);
					}

					for(i=0;i<strlen(pattern);i++)
					{
						printf("%c", ptr[i]);
					}
				}
				lineptr= lineptr + strlen(pattern)+pos;
				if(grep_i==1)
					ptr=strcasestr(lineptr, pattern);
				else
					ptr=strstr(lineptr, pattern);
				flag=0;
			}
			printf("%s", lineptr);
			if(grep_b==1)
				byteposition=ftell(file);
		}

	}
	int matchcount=0;
	if((grep_w==1 && (grep_c==1 || grep_m==1)))
	{
		while(fgets(line, sizeof(line), file))
		{
			if(grep_i==0 && strstr(line, pattern)==NULL)
			{
				if(grep_b==1)
					byteposition=ftell(file);
				continue;
			}
			else if(grep_i==1 && strcasestr(line, pattern)==NULL)
			{
				if(grep_b==1)
					byteposition=ftell(file);
				continue;
			}
			if(grep_i==0 && atleastoneword(line, pattern)==0)
			{
				if(grep_b==1)
					byteposition=ftell(file);
				continue;
			}
			else if(grep_i==1 && atleastonewordi(line, pattern)==0)
			{
				if(grep_b==1)
					byteposition=ftell(file);
				continue;
			}
			lineptr = line;
			matchcount++;
			if(grep_i==1)
				ptr=strcasestr(lineptr, pattern);
			else
				ptr=strstr(lineptr, pattern);
			if(ptr==line)
				flag=10;
			if(grep_H==1 && grep_c==0)
				printgrep(0, filename, 0);
			if(grep_b==1 && grep_c==0)
			{
				printf(GREEN "%d:",byteposition);
				printf(RESET "");
			}
			while(ptr!=NULL)
			{
				pos = ptr - lineptr;
				if((flag==10 && isalnum(ptr[strlen(pattern)])==0 && ptr[strlen(pattern)]!='_') || (isalnum(ptr[-1])==0 && isalnum(ptr[strlen(pattern)])==0 && ptr[-1]!='_' && ptr[strlen(pattern)]!='_')) 
				{
					if(grep_c==0)
					{
						for(i=0;i<pos;i++)
						{
							putchar(lineptr[i]);
						}
	
						for(i=0;i<strlen(pattern);i++)
						{
							printf(RED "%c", ptr[i]);
							printf(RESET "");
						}
					}
				}
				else
				{
					if(grep_c==0)
					{
						for(i=0;i<pos;i++)
						{
							putchar(lineptr[i]);	
						}
	
						for(i=0;i<strlen(pattern);i++)
						{
							printf("%c", ptr[i]);
						}
					}
				}
				lineptr= lineptr + strlen(pattern) + pos;
				if(grep_i==1)
					ptr=strcasestr(lineptr, pattern);
				else
					ptr=strstr(lineptr, pattern);
				flag=0;
			}
			if(grep_c==0)
				printf("%s", lineptr);
			if(grep_m==1 && m_number==matchcount)
				break;
			if(grep_b==1)
				byteposition=ftell(file);
		}
		if(grep_H==1 && grep_c==1)
			printgrep(0, filename, 0);
		if(grep_c==1)
			printf("%d\n", matchcount);
	}

}

int is_regular_file(char *path)
{
    struct stat p;
    stat(path, &p);
    return S_ISREG(p.st_mode);
}

int is_directory(char *path)
{
    struct stat p;
    stat(path, &p);
    return S_ISDIR(p.st_mode);
}

void grepr(char *path, int flag, char *pattern)
{
    DIR * dp = opendir(path);
    if(!dp) {
        perror(path);
        return;
    }
    struct dirent * dir;
    char nextdir[512];
    char regfile[512];
    while((dir = readdir(dp)))
        if(strncmp(dir->d_name, ".", 1))
        {
            sprintf(regfile, "%s/%s",path, dir->d_name);
            if(is_regular_file(regfile))
            {
            	if(grep_v==1)
            	{
            		grepv(pattern, regfile);
            	}
            	else if(grep_w==1)
            	{
            		grepw(pattern, regfile);
            	}
            	else
            	{
            		grep(pattern, regfile);
            	}
            }
         }
    closedir(dp);
    dp = opendir(path);
    while((dir = readdir(dp))) 
      if(strncmp(dir->d_name, ".", 1)) 
      {
        if(flag && dir->d_type == 4) {
            sprintf(nextdir, "%s/%s", path, dir->d_name);
            grepr(nextdir, 1, pattern);
        }
    }
    closedir(dp);
}
void grepv(char *pattern, char *filename)
{
	if(checkbinary(filename)==1)
	{
		return ;
	}
	FILE* file = fopen(filename, "r");
	if(file == NULL)
	{
		return ;
	}
	char line[1024];
	if(filename[0]=='.' && filename[1]=='/')
		filename+=2;
	if(grep_H == 0 && grep_h == 0)
	{
		grep_h=1;
	}
	int count=0;
	int byteposition=0;
	if(grep_c==0 && grep_m==0)
	{
		while(fgets(line, sizeof(line), file))
		{
			if(grep_i==0 && strstr(line, pattern)==NULL && grep_w==0)
			{
				if(grep_H==1)
					printgrep(0, filename, 0);
				if(grep_b==1)
				{
					printf(GREEN "%d:",byteposition);
					printf(RESET "");
				}
				printf("%s", line);
				if(grep_b==1)
					byteposition = ftell(file);
			}	
			else if(grep_i==1 && strcasestr(line, pattern)==NULL && grep_w==0)
			{
				if(grep_H==1)
					printgrep(0, filename, 0);
				if(grep_b==1)
				{
					printf(GREEN "%d:",byteposition);
					printf(RESET "");
				}
				printf("%s", line);
				if(grep_b==1)
					byteposition = ftell(file);
			}
			else if(grep_i==0 && grep_w==1 && atleastoneword(line, pattern)==0)
			{
				if(grep_H==1)
					printgrep(0, filename, 0);
				if(grep_b==1)
				{
					printf(GREEN "%d:",byteposition);
					printf(RESET "");
				}
				printf("%s", line);
				if(grep_b==1)
					byteposition = ftell(file);
			}	
			else if(grep_i==1 && grep_w==1 && atleastonewordi(line, pattern)==0)
			{
				if(grep_H==1)
					printgrep(0, filename, 0);
				if(grep_b==1)
				{
					printf(GREEN "%d:",byteposition);
					printf(RESET "");
				}
				printf("%s", line);
				if(grep_b==1)
					byteposition = ftell(file);
			}		
			else
			{
				if(grep_b==1)
					byteposition = ftell(file);
				continue;
			}
		}
	}
	if(((grep_c==1 || grep_m==1)))
	{
		while(fgets(line, sizeof(line), file))
		{
			if(grep_i==0 && strstr(line, pattern)==NULL)
			{
				count++;
				if(grep_H==1 && grep_c==0)
					printgrep(0, filename, 0);
				if(grep_b==1 && grep_c==0)
				{
					printf(GREEN "%d:",byteposition);
					printf(RESET "");
				}
				if(grep_c==0)
					printf("%s", line);
				if(grep_b==1)
					byteposition = ftell(file);
			}	
			else if(grep_i==1 && strcasestr(line, pattern)==NULL)
			{
				count++;
				if(grep_H==1 && grep_c==0)
					printgrep(0, filename, 0);
				if(grep_b==1 && grep_c==0)
				{
					printf(GREEN "%d:",byteposition);
					printf(RESET "");
				}
				if(grep_c==0)
					printf("%s", line);
				if(grep_b==1)
					byteposition = ftell(file);
			}
			else if(grep_i==0 && grep_w==1 && atleastoneword(line, pattern)==0)
			{
				count++;
				if(grep_H==1 && grep_c==0)
					printgrep(0, filename, 0);
				if(grep_b==1 && grep_c==0)
				{
					printf(GREEN "%d:",byteposition);
					printf(RESET "");
				}
				if(grep_c==0)
					printf("%s", line);
				if(grep_b==1)
					byteposition = ftell(file);
			}	
			else if(grep_i==1 && grep_w==1 && atleastonewordi(line, pattern)==0)
			{
				count++;
				if(grep_H==1 && grep_c==0)
					printgrep(0, filename, 0);
				if(grep_b==1 && grep_c==0)
				{
					printf(GREEN "%d:",byteposition);
					printf(RESET "");
				}
				if(grep_c==0)
					printf("%s", line);
				if(grep_b==1)
					byteposition = ftell(file);
			}		
			if(grep_m==1 && count==m_number)
				break;
			if(grep_b==1)
				byteposition = ftell(file);
		}
		if(grep_H==1 && grep_c==1)
			printgrep(0, filename, 0);
		if(grep_c==1)
			printf("%d\n", count);
	}
}
char *readfile(char *filename)
{
    FILE *file = fopen(filename, "r");
    char *text;
    int i = 0;
    char ch;

    if (file == NULL)
     {
		perror("Error");
		exit(0);	
     }		

    text = malloc(1024);

    while ((ch = fgetc(file)) != EOF)
    {
        text[i++] = ch;
    }
    text[i-1] = '\0';        
    return text;
}

int main(int argc, char *argv[])
{
	int option;
	int count=0;
	m_number=0;
	char *patternfile;
	char *filename;
	while((option = getopt(argc, argv, "vHhriqwf:cm:b"))!=-1)
		switch(option)
		{
			case 'v':
					grep_v=1;
					count++;
					break;
			case 'H':
					grep_H=1;
					count++;
					break;
			case 'h':
					grep_h=1;
					count++;
					break;
			case 'r':
					grep_r=1;
					count++;
					break;
			case 'i':
					grep_i=1;
					count++;
					break;
			case 'q':
					grep_q=1;
					count++;
					break;
			case 'f':
					grep_f=1;
					count+=2;
					patternfile = optarg;
					break;

			case 'w':
					grep_w=1;
					count++;
					break;
			case 'c':
					grep_c=1;
					count++;
					break;
			case 'm':
					grep_m=1;
					count+=2;
					m_number=atoi(optarg);
					break;
			case 'b':
					grep_b=1;
					count++;
					break;
			default:
					fprintf(stderr, "Usage: ./grep [OPTIONS]... PATTERN [FILENAME]...\n");
					exit(0);
			}

	if(argc<3)
	{
		fprintf(stderr, "Usage: ./grep [OPTIONS]... PATTERN [FILENAME]...\n");
		exit(0);
	}

	if(grep_h == 1 && grep_H == 1)
	{
		fprintf(stderr, "Usage: ./grep [OPTIONS]... PATTERN [FILENAME]...  -h and -H have opposite effects\n");
		exit(0);
	}

	char *pattern;


	if(grep_f==0)
	{
		pattern=argv[count+1];
		if(argv[count+2] == 0 && grep_r==0)
		{
			fprintf(stderr, "Usage: ./grep [OPTIONS]... PATTERN [FILENAME]...\n");
			exit(0);
		}
		if(grep_r==0)
		{
			filename=argv[count+2];
			int fp;
			fp=open(filename, O_RDONLY);
			if(fp==-1)
			{
				perror("Error");
				exit(0);
			}	
			close(fp);
		}

		if(grep_q==1)
		{
			exit(0);
		}
		
		if(m_number<0)
		{
			m_number=0;
			grep_m=0;
		}
	}

	else if(grep_f==1)
	{
		pattern = readfile(patternfile);
		if(argv[count+1] == 0 && grep_r==0)
		{
			fprintf(stderr, "Usage: ./grep [OPTIONS]... PATTERN [FILENAME]...\n");
			exit(0);
		}
		if(grep_r==0)
		{
			filename=argv[count+1];
			int fp;
			fp=open(filename, O_RDONLY);
			if(fp==-1)
			{
				perror("Error");
				exit(0);
			}	
			close(fp);
		}

		if(grep_q==1)
		{
			exit(0);
		}
		
		if(m_number<0)
		{
			m_number=0;
			grep_m=0;
		}

	}

	if((argv[count+1]==0 && grep_f==0))
	{
		fprintf(stderr, "Usage: ./grep [OPTIONS]... PATTERN [FILENAME]...\n");
		exit(0);
	}

	int i, j;

	

	if(grep_r==1)
	{
		if(grep_f==0 && argv[count+2]==0)
		{
			if(grep_h==0)
			{
				grep_H=1;
			}
			grepr(".", 1, pattern);
		}

		else if(grep_f==1)
		{
			if(argv[count+1]==0)
			{
				grep_h=0;
				grep_H=1;
				grepr(".", 1, pattern);
			}
			else
			{
					if(argc-count-1>1 && grep_h==0)
						grep_H=1;
					j=1;
					//printf("%s\n", argv[count+j]);
					while(argv[count+j]!=0)
					{
						strcpy(filename, argv[count+j]);
						if(filename[strlen(filename)-1]=='/')
						{
							i = strlen(filename)-1;
							while(filename[i]=='/')
								filename[i--]='\0';
						}
						if(is_regular_file(filename))
						{
								//printf("%s ", filename, pattern);
								if(grep_v==1)
								{
									grepv(pattern, filename);
								}
								else if(grep_w==1)
								{
								   	grepw(pattern, filename);   //works
								}
								else								
								{
									grep(pattern, filename);
								}
						}
						else
							grepr(filename, 1, pattern);
						j++;
					}
			}
		}
		else if(grep_f==0)
		{
				if(argc-count-2>1 && grep_h==0)
					grep_H=1;
				j=2;
				while(argv[count+j]!=0)
				{
					strcpy(filename, argv[count+j]);					
					i = strlen(filename)-1;
					if(filename[strlen(filename)-1]=='/')
					{
						i = strlen(filename)-1;
						while(filename[i]=='/')
							filename[i--]='\0';
					}
					if(is_regular_file(filename))
					{
							if(grep_v==1)
							{
								grepv(pattern, filename);
							}
							else if(grep_w==1)
							{
							   grepw(pattern, filename);   //works
							}
							else
							{
								grep(pattern, filename);
							}
					}
					else
						grepr(filename, 1, pattern);
					j++;
				}
		}
	}
	else if(grep_v==1)
	{
		i=0;
		if(grep_f == 1)
		{
			i=1;
			if(argc-count-1>1 && grep_h==0)
				grep_H=1;
			while(argv[count+i]!=0)
			{
				filename=argv[count+i];
				int fp;
				fp=open(filename, O_RDONLY);
				if(fp==-1)
				{
					perror(filename);
					i++;
					continue;
				}	
				close(fp);
				if(is_directory(filename))
				{
					printf("grep: %s: is a directory.\n", filename);
					i++;
					continue;
				}
				grepv(pattern, filename);
				i++;
			}
		}
		else
		{
			i=2;
			if(argc-count-2>1 && grep_h==0)
				grep_H=1;
			while(argv[count+i]!=0)
			{
				filename=argv[count+i];
				int fp;
				fp=open(filename, O_RDONLY);
				if(fp==-1)
				{
					perror(filename);
					i++;
					continue;
				}	
				close(fp);
				if(is_directory(filename))
				{
					printf("grep: %s: is a directory.\n", filename);
					i++;
					continue;
				}
				grepv(pattern, filename);
				i++;
			}
		}
	}


	else if(grep_w==1)
	{
		i=0;
		if(grep_f == 1)
		{
			if(argc-count-1>1 && grep_h==0)
				grep_H=1;
			i=1;
			while(argv[count+i]!=0)
			{
				filename=argv[count+i];
				int fp;
				fp=open(filename, O_RDONLY);
				if(fp==-1)
				{
					perror(filename);
					i++;
					continue;
				}
				if(is_directory(filename))
				{
					printf("grep: %s: is a directory.\n", filename);
					i++;
					continue;
				}	
				close(fp);
				grepw(pattern, filename);
				i++;
			}
		}
		else
		{
			i=2;
			if(argc-count-2>1 && grep_h==0)
				grep_H=1;
			while(argv[count+i]!=0)
			{
				filename=argv[count+i];
				int fp;
				fp=open(filename, O_RDONLY);
				if(fp==-1)
				{
					perror(filename);
					i++;
					continue;
				}	
				close(fp);
				if(is_directory(filename))
				{
					printf("grep: %s: is a directory.\n", filename);
					i++;
					continue;
				}
				grepw(pattern, filename);
				i++;
			}
		}
	}


	else
	{
		i=0;
		if(grep_f == 1)
		{
			i=1;
			if(argc-count-1>1 && grep_h==0)
				grep_H=1;
			while(argv[count+i]!=0)
			{
				filename=argv[count+i];
				int fp;
				fp=open(filename, O_RDONLY);
				if(fp==-1)
				{
					perror(filename);
					i++;
					continue;
				}	
				close(fp);
				if(is_directory(filename))
				{
					printf("grep: %s: is a directory.\n", filename);
					i++;
					continue;
				}
				grep(pattern, filename);
				i++;
			}
		}
		else
		{
			i=2;
			if(argc-count-2>1 && grep_h==0)
				grep_H=1;
			while(argv[count+i]!=0)
			{
				filename=argv[count+i];
				int fp;
				fp=open(filename, O_RDONLY);
				if(fp==-1)
				{
					perror(filename);
					i++;
					continue;
				}	
				close(fp);
				if(is_directory(filename))
				{
					printf("grep: %s: is a directory.\n", filename);
					i++;
					continue;
				}
				grep(pattern, filename);
				i++;
			}
		}
	}
	if(grep_f==1)
		free(pattern);
	return 1;
}
